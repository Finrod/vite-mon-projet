/** **************** Import Vue ******************/
import { createApp } from 'vue'
import { Router } from './router'

/** **************** Import Vue application ******************/
import App from './App.vue'

/** **************** Import Vue plugins ******************/

/** **************** Import style ******************/

/** **************** Create app ******************/
createApp(App).use(Router).mount('#app')
