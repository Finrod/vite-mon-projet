# Vue 3 + Typescript + Vite

This template should help get you started developing with Vue 3, Typescript in Vite.
It is worth mentioning that this model was made using the [Vitalis-template](https://github.com/Edsonalencar/Vitalis-template) model as a base.

## This starter template includes:

- [Vite](https://vitejs.dev/guide/)
- [Vue 3](https://staging.vuejs.org/guide/introduction.html)
- [Vue Router v4](https://github.com/vuejs/vue-router-next)
- [Typescript](https://www.typescriptlang.org/)
- [Eslint](https://eslint.org/docs/user-guide/getting-started)
- [Stylelint](https://stylelint.io/)
- [Prettier](https://prettier.io/docs/en/install.html)
- [SASS](https://sass-lang.com/guide)

## Recommended

- [VSCode](https://code.visualstudio.com/)
- [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Getting Started

```
npx degit ...
cd my-project
```

```
npm i
npm run dev
npm run build
```
