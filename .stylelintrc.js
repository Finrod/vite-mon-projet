module.exports = {
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-recess-order',
    'stylelint-config-standard-vue/scss'
  ],

  rules: {
    // Give weird errors
    'no-descending-specificity': null,
    // Conflicts with Prettier
    'declaration-colon-newline-after': null,
    'value-list-comma-newline-after': null,
    'string-quotes': 'single'
  },

  ignoreFiles: ['**/*.html']
}
